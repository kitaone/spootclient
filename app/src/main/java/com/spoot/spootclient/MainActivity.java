package com.spoot.spootclient;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerDragListener{

    LocationManager locationManager;
    double longitudeBest, latitudeBest;
    double longitudeGPS, latitudeGPS;
    double longitudeNetwork, latitudeNetwork;
    TextView longitudeValueBest, latitudeValueBest;
    TextView longitudeValueGPS, latitudeValueGPS;
    TextView longitudeValueNetwork, latitudeValueNetwork;
    TextView longFinish, latiFinish;

    String rideName;

    GoogleMap mGoogleMap;
    MapView mMapView;
    View mView;
    public static String serverAddres;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnSendRequest = (Button) findViewById(R.id.buttonSendRequest);
        btnSendRequest.setOnClickListener(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        longitudeValueNetwork = (TextView) findViewById(R.id.textVarLong);
        latitudeValueNetwork = (TextView) findViewById(R.id.textVarLat);
        longFinish = (TextView) findViewById(R.id.textVarLong2);
        latiFinish = (TextView) findViewById(R.id.textVarLat2);

        toggleNetworkUpdates();

        mMapView = (MapView) findViewById(R.id.map);
        if(mMapView != null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
        serverAddres = getResources().getString(R.string.spoot_server_local);

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
            try{
                //do your code here
                new GetUrlContentTask().execute(serverAddres + "checkclientstatus/" + rideName);
            } catch (Exception e) {
            }
            finally{
                handler.postDelayed(this, 10000);
            }
            }
        };
        runnable.run();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int resourceId = item.getItemId();

        if(resourceId == R.id.gcloud){
            CheckBox gcloud = (CheckBox) item.getActionView();
            if(item.isChecked()){
                serverAddres = getResources().getString(R.string.spoot_server_local);
                item.setChecked(false);
            }else{
                serverAddres = getResources().getString(R.string.spoot_server_gcloud);
                item.setChecked(true);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSendRequest: {
                String from = android.os.Build.MODEL.replace(" ", "_") + "_PU";
                String to = android.os.Build.MODEL.replace(" ", "_") + "_DE";
                rideName = from + to;
                //new GetUrlContentTask().execute("http://192.168.0.106:8080/spoot/spoot/waittostart/100/50.06133078/19.89246272");
                new GetUrlContentTask().execute(serverAddres + "addride/" + from + "/" + latitudeValueNetwork.getText() + "/" + longitudeValueNetwork.getText() + "/" + to + "/" + latiFinish.getText() + "/" + longFinish.getText());
                break;
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        mGoogleMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //googleMap.addMarker(new MarkerOptions().position(new LatLng(40.689247, -74.044502)).title("Test").snippet("Test2"));
        //CameraPosition camera = CameraPosition.builder().target(new LatLng(40.689247, -74.044502)).zoom(16).bearing(0).tilt(45).build();
        //googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(camera));
        googleMap.setOnMapLongClickListener(this);
        googleMap.setOnMarkerDragListener(this);
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Toast.makeText(MainActivity.this, android.os.Build.MODEL, Toast.LENGTH_SHORT).show();
        mGoogleMap.clear();
        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latitudeValueNetwork.getText().toString()), Double.valueOf(longitudeValueNetwork.getText().toString()))).title("Początek")).setDraggable(true);
        mGoogleMap.addMarker(new MarkerOptions().position(latLng).title("Koniec").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))).setDraggable(true);
        longFinish.setText(latLng.longitude + "");
        latiFinish.setText(latLng.latitude + "");
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        if(marker.getTitle().equals("Początek")){
            longitudeValueNetwork.setText(marker.getPosition().longitude + "");
            latitudeValueNetwork.setText(marker.getPosition().latitude + "");
        }else{
            longFinish.setText(marker.getPosition().longitude + "");
            latiFinish.setText(marker.getPosition().latitude + "");
        }
    }

    private class GetUrlContentTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... urls) {
            String content = "";
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                content = "";
                String line;
                while ((line = rd.readLine()) != null) {
                    content += line + "\n";
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return content;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(String result) {
            TextView textRequest = (TextView) findViewById(R.id.textRequest);
            JSONObject obj = null;
            try {
                obj = new JSONObject(result);
                if(obj.get("info").equals("addride")){
                    textRequest.setText(obj.getString("content"));
                } else if(obj.get("info").equals("checkclientstatus")){
                    textRequest.setText(obj.getString("content"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }


    public void toggleNetworkUpdates() {
        if (!checkLocation())
            return;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 60 * 1000, 10, locationListenerNetwork);
        Toast.makeText(this, "Network provider started running", Toast.LENGTH_LONG).show();
    }

    private final LocationListener locationListenerBest = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeBest = location.getLongitude();
            latitudeBest = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueBest.setText(longitudeBest + "");
                    latitudeValueBest.setText(latitudeBest + "");
                    Toast.makeText(MainActivity.this, "Best Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeNetwork = location.getLongitude();
            latitudeNetwork = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueNetwork.setText(longitudeNetwork + "");
                    latitudeValueNetwork.setText(latitudeNetwork + "");
                    mGoogleMap.clear();
                    mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(latitudeNetwork, longitudeNetwork)).title("Początek")).setDraggable(true);
                    if(!longFinish.getText().equals("") && !latiFinish.equals("")){
                        mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(latiFinish.getText().toString()), Double.valueOf(longFinish.getText().toString()))).title("Koniec").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))).setDraggable(true);
                    }
                    CameraPosition camera = CameraPosition.builder().target(new LatLng(latitudeNetwork, longitudeNetwork)).zoom(16).bearing(0).build();
                    mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(camera));
                    Toast.makeText(MainActivity.this, "Network Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final LocationListener locationListenerGPS = new LocationListener() {
        public void onLocationChanged(Location location) {
            longitudeGPS = location.getLongitude();
            latitudeGPS = location.getLatitude();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    longitudeValueGPS.setText(longitudeGPS + "");
                    latitudeValueGPS.setText(latitudeGPS + "");
                    Toast.makeText(MainActivity.this, "GPS Provider update", Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
}
